using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;

namespace castle_therm_mobile
{
    class ZoneInfo
    {
        public string Id { get; set; }
        [JsonProperty(PropertyName = "zone_id")]
        public int ZoneId { get; set; }
        [JsonProperty(PropertyName = "temp")]
        public float Temperature { get; set; }
        [JsonProperty(PropertyName = "target_temp")]
        public float TargetTemp { get; set; }
        [JsonProperty(PropertyName = "sys_mode")]
        public int SystemMode { get; set; }
        [JsonProperty(PropertyName = "sys_status")]
        public int SystemStatus { get; set; }
    }

    public class ZoneInfoWrapper : Java.Lang.Object
    {
        public ZoneInfoWrapper (ZoneInfo item)
        {
            ZoneInfo = item;
        }

        public ZoneInfo ZoneInfo { get; private set; }
    }
}