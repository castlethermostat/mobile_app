using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System;
using Android.App;
using Android.Views;
using Android.Widget;
using System.Collections.Generic;

namespace castle_therm_mobile 
{
    class ZoneInfoAdapter : BaseAdapter<ZoneInfo>
    {
        Activity activity;
        int layoutResourceId;
        List<ZoneInfo> zones = new List<ZoneInfo>();

        public ZoneInfoAdapter (Activity activity, int layoutResourceId)
        {
            this.activity = activity;
            this.layoutResourceId = layoutResourceId;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var row = convertView;
            var currentItem = this[position];
            CheckBox checkBox;

            if (row == null)
            {
                var inflater = activity.LayoutInflater;
                row = inflater.Inflate(layoutResourceId, parent, false);

                checkBox = row.FindViewById<CheckBox>(Resource.Id.check);

                checkBox.CheckedChange += async (sender, e) => {
                    var cbSender = sender as CheckBox;
                    if (cbSender != null && cbSender.Tag is ZoneInfoWrapper && cbSender.Checked)
                    {
                        cbSender.Enabled = false;
                        if (activity is ToDoActivity)
                            await ((ToDoActivity)activity).CheckItem((cbSender.Tag as ZoneInfoWrapper).ZoneInfo);
                    }
                };
            }
            else
                checkBox = row.FindViewById<CheckBox>(Resource.Id.checkZoneInfo);

            checkBox.Text = currentItem.Text;
            checkBox.Checked = false;
            checkBox.Enabled = true;
            checkBox.Tag = new ZoneInfoWrapper(currentItem);

            return row;
        }
        public void Add(ZoneInfo item)
        {
            zones.Add(item);
            NotifyDataSetChanged();
        }

        public void Clear()
        {
            zones.Clear();
            NotifyDataSetChanged();
        }

        public void Remove(ZoneInfo item)
        {
            zones.Remove(item);
            NotifyDataSetChanged();
        }

        #region implemented abstract members of BaseAdapter

        public override long GetItemId(int position)
        {
            return position;
        }

        public override int Count
        {
            get
            {
                return zones.Count;
            }
        }

        public override ZoneInfo this[int position]
        {
            get
            {
                return zones[position];
            }
        }

        #endregion
    }


}
