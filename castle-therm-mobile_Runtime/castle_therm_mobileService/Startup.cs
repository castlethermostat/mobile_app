using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(castle_therm_mobileService.Startup))]

namespace castle_therm_mobileService
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureMobileApp(app);
        }
    }
}