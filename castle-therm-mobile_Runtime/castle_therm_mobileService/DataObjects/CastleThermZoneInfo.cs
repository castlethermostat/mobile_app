﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Azure.Mobile.Server;


namespace castle_therm_mobileService.DataObjects
{
    public class CastleThermZoneInfo : EntityData
    {
        public int ZoneId { get; set; }
        public float Temperature { get; set; }
        public float TargetTemp { get; set; }
        public int SystemMode { get; set; }
        public int SystemStatus { get; set; }
    }
}