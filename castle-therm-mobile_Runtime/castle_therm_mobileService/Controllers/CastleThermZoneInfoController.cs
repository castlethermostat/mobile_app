﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.Azure.Mobile.Server;
using castle_therm_mobileService.DataObjects;
using castle_therm_mobileService.Models;

namespace castle_therm_mobileService.Controllers
{
    public class CastleThermZoneInfoController : TableController<CastleThermZoneInfo>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            castle_therm_mobileContext context = new castle_therm_mobileContext();
            DomainManager = new EntityDomainManager<CastleThermZoneInfo>(context, Request);
        }

        // GET tables/CastleThermZoneInfo
        public IQueryable<CastleThermZoneInfo> GetAllCastleThermZoneInfo()
        {
            return Query(); 
        }

        // GET tables/CastleThermZoneInfo/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<CastleThermZoneInfo> GetCastleThermZoneInfo(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/CastleThermZoneInfo/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<CastleThermZoneInfo> PatchCastleThermZoneInfo(string id, Delta<CastleThermZoneInfo> patch)
        {
             return UpdateAsync(id, patch);
        }

        // POST tables/CastleThermZoneInfo
        public async Task<IHttpActionResult> PostCastleThermZoneInfo(CastleThermZoneInfo item)
        {
            CastleThermZoneInfo current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/CastleThermZoneInfo/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeleteCastleThermZoneInfo(string id)
        {
             return DeleteAsync(id);
        }
    }
}
